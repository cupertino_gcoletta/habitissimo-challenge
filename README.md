# Habitissimo Challenge

Desarrollo de Landing Page para solicitud de presupuestos - Habitissimo Challenge.


#### Frontend:

Para la maquetación he utilizado HTML, SASS, Bootstrap y Fontawesome para los iconos.
Hay una prueba subida en: [http://cupertino.es/_proyectos/gcg_frontendchallenge/presupuestos/](http://cupertino.es/_proyectos/gcg_frontendchallenge/presupuestos/)
El funcionamiento del Layout es responsive:

[Captura de pantalla 1](http://cupertino.es/_proyectos/gcg_frontendchallenge/capturas/Captura_1.png).

[Captura de pantalla 2](http://cupertino.es/_proyectos/gcg_frontendchallenge/capturas/Captura_2.png).

[Captura de pantalla 3](http://cupertino.es/_proyectos/gcg_frontendchallenge/capturas/Captura_3.png).

[Captura de pantalla 4](http://cupertino.es/_proyectos/gcg_frontendchallenge/capturas/Captura_4.png).

También he utilizado jQuery para el formulario y Grunt para la automatización de tareas.


#### Api:

Se encuentra realizado en PHP utilizando: SLIM y ELOQUENT.

Los datos se envían en formato JSON.

Ejemplos:


  - Lista de categorias. Ejemplo: GET:[http://cupertino.es/_proyectos/gcg_frontendchallenge/api/category/list](http://cupertino.es/_proyectos/gcg_frontendchallenge/api/category/list)
  - Lista de subcategorias. Ejemplo: GET:[http://cupertino.es/_proyectos/gcg_frontendchallenge/api/category/list/{id-category}](http://cupertino.es/_proyectos/gcg_frontendchallenge/api/category/list/001-5)
  - Validación de email: GET: [http://cupertino.es/_proyectos/gcg_frontendchallenge/api/emailvalidation/{email}](http://cupertino.es/_proyectos/gcg_frontendchallenge/api/emailvalidation/gabrie@hotmail.es)

Ejemplo de respuesta de la validación de email:
{
    "status": "error",
    "email_valid": true,
    "is_hotmail": true
}

Las posibilidades de respuesta son las siguientes:

"status": [ok, error]

"email_valid": [true, false]

"is_hotmail": [true, false]


Configuración:

* api/config/credentials.php : Configurar los datos de conexión

* api/database/create.sql : Script para crear tabla de categorías e importar los datos


#### Tareas pendientes:

Tenía la intensión de realizar algunas tareas adicionales. Como ya os habrá comentado Eva, he tenido una semana complicada. Os ruego me disculpen tanto retraso.

Para el front también quería utilizar SLIM y TWIG con patrón MVC. Pero al no coger contenidos de BBDD no lo considero muy necesario para este ejemplo.

En la maquetación HTML/CSS falta contemplar los navegadores antiguos. He dejado comentarios en el código.

Algo importante que me ha quedado por realizar es la prueba unitaria del código jQuery con qUnit.


Muchas gracias!


