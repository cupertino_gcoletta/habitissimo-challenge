# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.24)
# Base de datos: frontend_challenge
# Tiempo de Generación: 2018-06-24 11:12:41 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `pk` int(4) NOT NULL AUTO_INCREMENT,
  `category_id` varchar(12) COLLATE utf8_bin NOT NULL DEFAULT '',
  `parent_id` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `normalized_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `children` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`pk`, `category_id`, `parent_id`, `name`, `normalized_name`, `description`, `children`, `created_at`)
VALUES
	(1,'001-11',NULL,'Construcción','construccion','Casas, Edificios, Piscinas...',NULL,NULL),
	(2,'001-2',NULL,'Reforma','reforma','Baños, Cocinas, Comunidades...',NULL,NULL),
	(3,'001-10',NULL,'Mudanzas','mudanzas','Viviendas, Oficinas, Locales...',NULL,NULL),
	(4,'001-1',NULL,'Técnicos','tecnicos','Arquitectos, Licencias, Decoradores...',NULL,NULL),
	(5,'001-4',NULL,'Obras Menores','obras-menores','Albañiles, Carpinteros, Pintores...',NULL,NULL),
	(6,'001-5',NULL,'Instaladores','instaladores','Ascensores, Calefacción, Electricistas...',NULL,NULL),
	(7,'001-6',NULL,'Mantenimiento','mantenimiento','Jardineros, Limpieza, Manitas...',NULL,NULL),
	(8,'001-7',NULL,'Tiendas','tiendas','Maquinaria, Materiales, Decoración...',NULL,NULL),
	(9,'002-1','001-1','Arquitectos','arquitectos',NULL,NULL,NULL),
	(10,'002-160','001-1','Licencias','licencias',NULL,NULL,NULL),
	(11,'002-57','001-1','Decoradores','decoradores',NULL,NULL,NULL),
	(12,'002-161','001-1','Inspección Técnica Edificios ITE','inspeccion-tecnica-edificios-ite',NULL,NULL,NULL),
	(13,'002-55','001-1','Topógrafos','topografos',NULL,NULL,NULL),
	(14,'002-2','001-1','Arquitectos Técnicos','arquitectos-tecnicos',NULL,NULL,NULL),
	(15,'002-7','001-1','Peritos','peritos',NULL,NULL,NULL),
	(16,'002-181','001-1','Certificaciones Energéticas','certificaciones-energeticas',NULL,NULL,NULL),
	(17,'002-8','001-1','Geólogos','geologos',NULL,NULL,NULL),
	(18,'002-58','001-1','Ingenieros','ingenieros',NULL,NULL,NULL),
	(19,'002-56','001-1','Paisajistas','paisajistas',NULL,NULL,NULL),
	(20,'002-3','001-1','Delineantes','delineantes',NULL,NULL,NULL),
	(21,'002-10','001-2','Reformas Viviendas','reformas-viviendas',NULL,NULL,NULL),
	(22,'002-12','001-2','Reformas Baños','reformas-banos',NULL,NULL,NULL),
	(23,'002-11','001-2','Reformas Cocinas','reformas-cocinas',NULL,NULL,NULL),
	(24,'002-17','001-2','Reformas Locales Comerciales','reformas-locales-comerciales',NULL,NULL,NULL),
	(25,'002-14','001-2','Rehabilitación Fachadas','rehabilitacion-fachadas',NULL,NULL,NULL),
	(26,'002-13','001-2','Reformas Comunidades','reformas-comunidades',NULL,NULL,NULL),
	(27,'002-157','001-2','Reformas Piscinas','reformas-piscinas',NULL,NULL,NULL),
	(28,'002-21','001-2','Rehabilitación edificios','rehabilitacion-edificios',NULL,NULL,NULL),
	(29,'002-18','001-2','Reformas Oficinas','reformas-oficinas',NULL,NULL,NULL),
	(30,'002-19','001-2','Reformas Hoteles','reformas-hoteles',NULL,NULL,NULL),
	(31,'002-20','001-2','Reformas Naves Industriales','reformas-naves-industriales',NULL,NULL,NULL),
	(32,'002-33','001-4','Pintores','pintores',NULL,NULL,NULL),
	(33,'002-41','001-4','Parquetistas','parquetistas',NULL,NULL,NULL),
	(34,'002-116','001-4','Carpintería PVC','carpinteria-pvc',NULL,NULL,NULL),
	(35,'002-36','001-4','Carpintería Aluminio','carpinteria-aluminio',NULL,NULL,NULL),
	(36,'002-28','001-4','Albañiles','albaniles',NULL,NULL,NULL),
	(37,'002-118','001-4','Tejados','tejados',NULL,NULL,NULL),
	(38,'002-35','001-4','Carpinteros','carpinteros',NULL,NULL,NULL),
	(39,'002-15','001-4','Impermeabilizaciones','impermeabilizaciones',NULL,NULL,NULL),
	(40,'002-124','001-4','Armarios','armarios',NULL,NULL,NULL),
	(41,'002-177','001-4','Hormigón Impreso','hormigon-impreso',NULL,NULL,NULL),
	(42,'002-30','001-4','Fontaneros','fontaneros',NULL,NULL,NULL),
	(43,'002-37','001-4','Carpintería Metálica','carpinteria-metalica',NULL,NULL,NULL),
	(44,'002-68','001-4','Pladur','pladur',NULL,NULL,NULL),
	(45,'002-123','001-4','Tapiceros','tapiceros',NULL,NULL,NULL),
	(46,'002-44','001-4','Aislamiento','aislamiento',NULL,NULL,NULL),
	(47,'002-39','001-4','Cristaleros','cristaleros',NULL,NULL,NULL),
	(48,'002-45','001-4','Insonorización','insonorizacion',NULL,NULL,NULL),
	(49,'002-40','001-4','Cerrajeros','cerrajeros',NULL,NULL,NULL),
	(50,'002-175','001-4','Microcemento','microcemento',NULL,NULL,NULL),
	(51,'002-176','001-4','Pavimentos Continuos','pavimentos-continuos',NULL,NULL,NULL),
	(52,'002-121','001-4','Poceros','poceros',NULL,NULL,NULL),
	(53,'002-122','001-4','Trabajos Verticales','trabajos-verticales',NULL,NULL,NULL),
	(54,'002-34','001-4','Yeseros','yeseros',NULL,NULL,NULL),
	(55,'002-43','001-4','Marmolistas','marmolistas',NULL,NULL,NULL),
	(56,'002-66','001-5','Aire Acondicionado','aire-acondicionado',NULL,NULL,NULL),
	(57,'002-65','001-5','Calefacción','calefaccion',NULL,NULL,NULL),
	(58,'002-32','001-5','Electricistas','electricistas',NULL,NULL,NULL),
	(59,'002-54','001-5','Ascensores','ascensores',NULL,NULL,NULL),
	(60,'002-62','001-5','Toldos','toldos',NULL,NULL,NULL),
	(61,'002-120','001-5','Puertas Garaje','puertas-garaje',NULL,NULL,NULL),
	(62,'002-114','001-5','Placas Solares','placas-solares',NULL,NULL,NULL),
	(63,'002-162','001-5','Energías Renovables','energias-renovables',NULL,NULL,NULL),
	(64,'002-31','001-5','Gas','gas',NULL,NULL,NULL),
	(65,'002-119','001-5','Porteros Automáticos','porteros-automaticos',NULL,NULL,NULL),
	(66,'002-83','001-5','Chimeneas','chimeneas',NULL,NULL,NULL),
	(67,'002-64','001-5','Contra Incendios','contra-incendios',NULL,NULL,NULL),
	(68,'002-47','001-5','Antenas','antenas',NULL,NULL,NULL),
	(69,'002-46','001-5','Alarmas','alarmas',NULL,NULL,NULL),
	(70,'002-53','001-5','Domótica','domotica',NULL,NULL,NULL),
	(71,'002-51','001-5','Rótulos','rotulos',NULL,NULL,NULL),
	(72,'002-61','001-5','Telecomunicaciones','telecomunicaciones',NULL,NULL,NULL),
	(73,'002-77','001-6','Limpieza','limpieza',NULL,NULL,NULL),
	(74,'002-9','001-6','Jardineros','jardineros',NULL,NULL,NULL),
	(75,'002-82','001-6','Control Plagas','control-plagas',NULL,NULL,NULL),
	(76,'002-178','001-6','Pulir Suelos','pulir-suelos',NULL,NULL,NULL),
	(77,'002-128','001-6','Mantenimiento Ascensores','mantenimiento-ascensores',NULL,NULL,NULL),
	(78,'002-141','001-6','Manitas','manitas',NULL,NULL,NULL),
	(79,'002-164','001-6','Mantenimiento Comunidades','mantenimiento-comunidades',NULL,NULL,NULL),
	(80,'002-129','001-6','Mantenimiento Piscinas','mantenimiento-piscinas',NULL,NULL,NULL),
	(81,'002-81','001-6','Fosas Sépticas','fosas-septicas',NULL,NULL,NULL),
	(82,'002-95','001-7','Muebles','muebles',NULL,NULL,NULL),
	(83,'002-125','001-7','Artículos Decoración','articulos-decoracion',NULL,NULL,NULL),
	(84,'002-96','001-7','Electrodomésticos','electrodomesticos',NULL,NULL,NULL),
	(85,'002-180','001-7','Alquiler Maquinaria y Herramientas','alquiler-maquinaria-y-herramientas',NULL,NULL,NULL),
	(86,'002-126','001-7','Materiales Construcción','materiales-construccion',NULL,NULL,NULL),
	(87,'002-134','001-10','Mudanzas Viviendas','mudanzas-viviendas',NULL,NULL,NULL),
	(88,'002-140','001-10','Guardamuebles','guardamuebles',NULL,NULL,NULL),
	(89,'002-136','001-10','Mudanzas Oficinas','mudanzas-oficinas',NULL,NULL,NULL),
	(90,'002-135','001-10','Mudanzas Locales Comerciales','mudanzas-locales-comerciales',NULL,NULL,NULL),
	(91,'002-138','001-10','Mudanzas Industrias','mudanzas-industrias',NULL,NULL,NULL),
	(92,'002-22','001-11','Construcción Casas','construccion-casas',NULL,NULL,NULL),
	(93,'002-50','001-11','Construcción Piscinas','construccion-piscinas',NULL,NULL,NULL),
	(94,'002-151','001-11','Construcción Muros','construccion-muros',NULL,NULL,NULL),
	(95,'002-145','001-11','Construcción Casas Prefabricadas','construccion-casas-prefabricadas',NULL,NULL,NULL),
	(96,'002-152','001-11','Construcción Naves Industriales','construccion-naves-industriales',NULL,NULL,NULL),
	(97,'002-146','001-11','Construcción Edificios','construccion-edificios',NULL,NULL,NULL),
	(98,'002-154','001-11','Derribos','derribos',NULL,NULL,NULL),
	(99,'002-155','001-11','Excavaciones','excavaciones',NULL,NULL,NULL),
	(100,'002-149','001-11','Construcción Instalaciones Deportivas','construccion-instalaciones-deportivas',NULL,NULL,NULL),
	(101,'002-148','001-11','Construcción Garajes','construccion-garajes',NULL,NULL,NULL),
	(102,'002-156','001-11','Obra Civil','obra-civil',NULL,NULL,NULL),
	(103,'002-153','001-11','Construcción Saunas','construccion-saunas',NULL,NULL,NULL),
	(104,'002-150','001-11','Construcción Jacuzzis','construccion-jacuzzis',NULL,NULL,NULL);

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
