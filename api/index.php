<?php

require 'vendor/autoload.php';
include 'bootstrap.php';

use Challenge\Models\Category;
use Challenge\Middleware\Logging as ChallengeLogging;

$app = new \Slim\App();
$app->add(new ChallengeLogging());


// Category list
$app->group('/category/list', function () {
  $this->map(['GET'], '', function ($request, $response, $args) {
    $_category = new Category();

    $categories = $_category->where('parent_id', '=', NULL)
      ->orderBy('name', 'asc')
      ->get();

    $payload = [];
    foreach($categories as $_data) {
      $payload[] = $_data->output();
    }

    return $response->withStatus(200)->withJson($payload);
  })->setName('get_categories');
});


// Subcategory list from category parent_id
$app->group('/category/list/{parent_id}', function () {
  $this->map(['GET'], '', function ($request, $response, $args) {
    $_category = new Category();

    $categories = $_category->where('parent_id', '=', $args['parent_id'])
      ->orderBy('name', 'asc')
      ->get();

    $payload = [];
    foreach($categories as $_data) {
      $payload[] = $_data->output();
    }

    return $response->withStatus(200)->withJson($payload);
  })->setName('get_subcategories');
});


// Email validation
$app->group('/emailvalidation/{email}', function () {
  $this->map(['GET'], '', function ($request, $response, $args) {

    $email = $args['email'];
    $email_valid = false;
    $is_hotmail = false;
    $validation_status = "error";

    // Remove all illegal characters from email
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);

    // Validate e-mail
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $email_valid = true;
    }

    // Detect if email is hotmail
    if (preg_match('/(.*)@(hotmail)\.(.*)/', $email) != false) {
      $is_hotmail = true;
    }

    if (($email_valid == true) && ($is_hotmail == false)) {
      $validation_status = "ok";
    }

    $payload = ['status'      => $validation_status,
                'email_valid' => $email_valid,
                'is_hotmail'  => $is_hotmail];

    return $response->withStatus(200)->withJson($payload);
  })->setName('get_emailvalidation');
});


// Response to requests a bad url.
$app->add(function ($request, $response, $next) use ($c) {

  $response = $next($request, $response);
  // Check if the response should render a 404
  if (404 === $response->getStatusCode() &&
      0   === $response->getBody()->getSize()
  ) {
    // A 404 should be invoked
    $handler = $c['notFoundHandler'];
    return $handler($request, $response);
  }
  // Any other request, pass on current response
  return $response;
});

// Run app
$app->run();
