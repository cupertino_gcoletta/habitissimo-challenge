<?php

namespace Challenge\Models;

class Category extends \Illuminate\Database\Eloquent\Model
{
  public function output()
  {
    $output = [];
    $output['id']               = $this->category_id;
    $output['parent_id']        = $this->parent_id;
    $output['name']             = $this->name;
    $output['normalized_name']  = $this->normalized_name;
    $output['description']      = $this->description;
    $output['children']         = $this->children;

    return $output;
  }
}
