module.exports = function(grunt) {

	// load all grunt tasks
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

        copy: {
            main: {
                expand: true,
                cwd: 'src/',
                src: ["**", "!assets/sass/**", "!assets/js/*.js"],
                dest: 'dist/'
            }
        },

        sass: {
                dist: {
                    options: {
                        style: 'compressed',
                        noCache: true
                    },
                    files: [{
                        expand: true,
                        cwd:  "src/assets/sass",
                        src:  ["*.scss"],
                        dest: "src/assets/css",
                        ext:  ".css"
                    }]
                }
    		},

        concat: {
            options: {
                separator: '',
                stripBanners: true,
                 banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            dist: {
                src: ['src/assets/js/*.js', "!src/assets/js/*.min.js"],
                dest: 'src/assets/js/main.min.js'
            }
        },

        uglify: {
            options: {
                manage: false,
                preserveComments: 'all' //preserve all comments on JS files
            },
            my_target: {
                files: {
                    'dist/assets/js/main.min.js' : ['src/assets/js/main.min.js']
                }
            }
        },

        cssmin: {
            my_target: {
                files: [{
                    expand: true,
                    cwd: 'src/assets/css/',
                    src: ['*.css', '!*.min.css'],
                    dest: 'src/assets/css/',
                    ext: '.min.css'
                }]
            }
        },

        watch: {
            options: {
                nospawn: true,
                livereload: true
            },
            sass: {
                files: ['src/assets/sass/**/*.scss'],
                tasks: ['sass']
            },
            cssmin: {
                files: ['src/assets/sass/**/*.scss'],
                tasks: ['cssmin']
            },
            concat: {
                files: ['src/assets/js/**/*.js'],
                tasks: ['concat']
            },
            uglify: {
                files: ['src/assets/js/**/*.js'],
                tasks: ['uglify']
            },
            copy: {
                files: ['src/**'],
                tasks: ['copy:main']
            }
        }
	});


    // Load the plugin that provides the "compass" task.
    grunt.loadNpmTasks('grunt-contrib-compass');

    // Load the plugin that provides the "watch" task.
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Load the plugin that provides the "sass" task.
    grunt.loadNpmTasks('grunt-contrib-sass');

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');

    // Load the plugin that provides the "concat" task.
    grunt.loadNpmTasks('grunt-contrib-concat');

    // Load the plugin that provides the "cssmin" task.
    grunt.loadNpmTasks('grunt-contrib-cssmin');

	grunt.registerTask('default', ['watch']);
};
