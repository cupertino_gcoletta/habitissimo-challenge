// Main

// Form Class
var formBudgetRequest = function(form_id){

  // Variables accessible
  var vars = {
    form_id : form_id,
    current_step : 0,
    category_title : $('#category-title'),
    form : {
      form : $(form_id),
      btn_next_step : $(form_id).find('#btn_next_step'),
      btn_prev_step : $(form_id).find('#btn_prev_step'),
      steps : $(form_id).find('.step'),
      wrapper_step_point : $('.wrapper-step-point'),
      category : $('#category'),
      wrapper_sub_category : $('.wrapper-subcategory'),
      sub_category : $('#subcategory'),
      email : $('#email'),
      email_validation : $('.email-validation'),
    },
    name: {
      description : 'description',
      estimated_date : 'estimated-date',
      category : 'category',
      sub_category : 'subcategory',
      price_preferred : 'price-preferred',
      name : 'name',
      email : 'email',
      phone : 'phone',
    },
    text : {
      btn_next: 'Continuar',
      btn_prev: 'Volver',
      btn_new: 'Solicita otro presupuesto!',
    },
    budget : {
      description : '',
      estimated_date : '',
      category : '',
      sub_category : '',
      price_preferred : '',
      name : '',
      email : '',
      phone : '',
    },
    api : {
      url_category : 'http://localhost/habitissimo-challenge/api/category/list',
      url_sub_category : 'http://localhost/habitissimo-challenge/api/category/list/',
      url_email_validation : 'http://localhost/habitissimo-challenge/api/emailvalidation/'
    }
  };

  // Show step
  var showStep = function(stepNumber) {
    vars.form.steps.eq(stepNumber).css('display','block');

    // fix the view of prev buttons
    if ((stepNumber == 0) || (stepNumber == (vars.form.steps.length - 1))) {
      vars.form.btn_prev_step.hide();
    } else {
      vars.form.btn_prev_step.show();
    }

    // buttons name
    vars.form.btn_prev_step.html(vars.text.btn_prev);
    if (stepNumber == (vars.form.steps.length - 1)) {
      $(vars.form_id).find('.step-point').eq(vars.form.steps.length-1).addClass('finish');
      vars.form.btn_next_step.html(vars.text.btn_new);
      console.log(vars.budget);
    } else {
      vars.form.btn_next_step.html(vars.text.btn_next);
    }

    // displays the correct step indicator
    fixStepIndicator(stepNumber);
  };

  // visual steps indicators
  var fixStepIndicator = function(stepNumber) {
    if (vars.form.wrapper_step_point.find('.step-point').length == 0) {
      var content = '', steps_length = vars.form.steps.length;

      for (i = 0; i < steps_length; i++) {
        content += '<span class="step-point"></span>';
      }
      vars.form.wrapper_step_point.html(content);
    }

    var steps_point = vars.form.wrapper_step_point.find('.step-point');

    steps_point.removeClass("active");
    steps_point.eq(stepNumber).addClass("active");
  };

  // move to prev / next step
  var nextPrev = function(increment) {
    if (increment == 1 && !validateForm()) return false;

    vars.form.steps[vars.current_step].style.display = "none";
    vars.current_step = vars.current_step + increment;

    if (vars.current_step >= vars.form.steps.length) {
      vars.form.btn_next_step.unbind('click');
      newBudget();
      return false;
    }

    showStep(vars.current_step);
  };

  // validate form
  var validateForm = function() {
    var x, y, i, valid = true;

    //vars.form.steps
    x = document.getElementsByClassName('step');

    var inputs_text_area = vars.form.steps.eq(vars.current_step).find('textarea');
    for (i = 0; i < inputs_text_area.length; i++) {
      inputs_text_area.eq(i).removeClass('is-invalid');
      if (inputs_text_area.eq(i).val() == "") {
        inputs_text_area.eq(i).addClass('is-invalid');
        valid = false;
      }
    }

    var inputs_radio = vars.form.steps.eq(vars.current_step).find('input[type=radio]');
    var radio_name = '';
    for (i = 0; i < inputs_radio.length; i++) {
      inputs_radio.eq(i).removeClass('is-invalid');
      if (!$('input[name="'+inputs_radio.eq(i).attr('name')+'"]').is(':checked')) {
        inputs_radio.eq(i).addClass('is-invalid');
        valid = false;
      }
    }

    var inputs_text= vars.form.steps.eq(vars.current_step).find('input[type=text]');
    for (i = 0; i < inputs_text.length; i++) {
      inputs_text.eq(i).removeClass('is-invalid');
      if (inputs_text.eq(i).val() == "") {
        inputs_text.eq(i).addClass('is-invalid');
        valid = false;
      }
      // email validation
      if ((inputs_text.eq(i).attr('name') == 'email') && (inputs_text.eq(i).val() != "")) {
        $.ajax( {
          url: vars.api.url_email_validation + inputs_text.eq(i).val(),
          dataType: 'json',
          type: 'GET',
          async: false,
          cache: false,
          success: function(data) {
            if (data.status == 'error') { // error , ok
              valid = false;
              if (data.email_valid == false) { // true , false
                vars.form.email_validation.html('Danos una dirección de email válida');
              }
              if (data.is_hotmail == true) { // true , false
                vars.form.email_validation.html('Danos una dirección de email que no sea de hotmail');
              }
              vars.form.email.addClass('is-invalid');
            }
          },
          error: function() {
            return 'error';
          }
        });
      }
    }

    var selects = vars.form.steps.eq(vars.current_step).find('select');
    for (i = 0; i < selects.length; i++) {
      selects.eq(i).removeClass('is-invalid');
      if (selects.eq(i).val() == "") {
        selects.eq(i).addClass('is-invalid');
        valid = false;
      }
    }

    if (valid) {
      $(vars.form_id).find('.step-point').eq(vars.current_step).addClass('finish');
      saveBudget(true);
      scrollTopForm();
    }
    return valid;
  };

  // scrollTopForm
  var scrollTopForm = function(status) {
    $('html, body').animate({
      scrollTop: $('.wrapper_steps').offset().top
    }, 500, 'linear');
  };

  // save budget , true = save | false = delete
  var saveBudget = function(status) {
    if (status == true) {
      vars.budget.description = $('#'+vars.name.description).val();
      vars.budget.estimated_date = $('input[name='+vars.name.estimated_date+']:checked').val();
      vars.budget.category = $('#'+vars.name.category).val();
      vars.budget.sub_category = $('input[name='+vars.name.sub_category+']:checked').val();
      vars.budget.price_preferred = $('input[name='+vars.name.price_preferred+']:checked').val();
      vars.budget.name = $('#'+vars.name.name).val();
      vars.budget.email = $('#'+vars.name.email).val();
      vars.budget.phone = $('#'+vars.name.phone).val();
    } else {
      $('#'+vars.name.description).val('');
      $('input[name='+vars.name.estimated_date+']:checked').prop('checked', false);
      $('#'+vars.name.category).val('');
      changeCategory($('#'+vars.name.category).val());
      $('input[name='+vars.name.price_preferred+']:checked').prop('checked', false);
      $('#'+vars.name.name).val('');
      $('#'+vars.name.email).val('');
      $('#'+vars.name.phone).val('');
    }
  };

  // new budget
  var newBudget = function() {
    saveBudget(false);
    vars.category_title.html('Pide presupuestos');
    $(vars.form_id).find('.step-point').removeClass('active');
    $(vars.form_id).find('.step-point').removeClass('finish');
    bindEvents();
    vars.current_step = 0;
    showStep(0);
  };

  // bind events
  var bindEvents = function(stepNumber) {
    vars.form.btn_next_step.unbind('click');
    vars.form.btn_next_step.on('click', function(e) {
      e.preventDefault();
      nextPrev(1);
    });

    vars.form.btn_prev_step.unbind('click');
    vars.form.btn_prev_step.on('click', function(e) {
      e.preventDefault();
      nextPrev(-1);
    });

    vars.form.category.unbind('change');
    vars.form.category.on('change', function(e) {
      e.preventDefault();
      changeCategory($(this).val());
    });
  };

  // populate category
  var populateCategory = function() {
    $.ajax( {
      url: vars.api.url_category,
      dataType: 'json',
      type: 'GET',
      success: function(data) {
        var category_length = data.length;
        var content = '';

        for (var i = 0; i < category_length; ++i) {
          content = '<option value="' + data[i].id + '">' + data[i].name + '</option>';
          vars.form.category.append(content);
        }
      },
      error: function() {
        vars.form.category.attr('disabled', 'true');
      }
    });
  };

  // change category
  var changeCategory = function(category_id) {
    // get subcategory
    if (category_id != '') {
      // change tittle
      vars.budget.category = $('#category option:selected').text();
      vars.category_title.html('Pide presupuestos de ' + vars.budget.category);
      vars.form.category.removeClass('is-invalid');

      $.ajax( {
        url: vars.api.url_sub_category + category_id,
        dataType: 'json',
        type: 'GET',
        success: function(data) {
          var sub_category_length = data.length, content = '', text_validation = '';
          vars.form.sub_category.html(content);

          for (var i = 0; i < sub_category_length; ++i) {
            if (i == (sub_category_length - 1)) {
              text_validation = '<div class="invalid-feedback">Selecciona la sub-categoria del presupuesto</div>';
            }
            content = '<div class="custom-control custom-radio">' +
              '<input type="radio" id="subcategory-'+data[i].id+'" name="subcategory" class="custom-control-input" value="'+data[i].id+'">' +
              '<label class="custom-control-label" for="subcategory-'+data[i].id+'">'+data[i].name+'</label>' + text_validation +
            '</div>';
            vars.form.sub_category.append(content);
          }
          vars.form.wrapper_sub_category.show();
        },
        error: function() {
          vars.form.wrapper_sub_category.hide();
        }
      });

    } else {
      vars.form.sub_category.html('');
    }
  };

  // Constructor
  this.construct = function(form_id){
    $.extend(vars , form_id);
    populateCategory();
    bindEvents();
    showStep(vars.current_step);
  };

  // class instantiated
  this.construct(form_id);
};

$(document).ready(function(){
  // Init form
  new formBudgetRequest('#budget-request');
});
